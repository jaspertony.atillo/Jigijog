package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Featured;

import java.util.ArrayList;

public class FeaturedGridAdapter extends RecyclerView.Adapter<FeaturedGridAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Featured> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public FeaturedGridAdapter(Context context, ArrayList<Featured> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_featured_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Featured featuredMddel = mList.get(position);
        ImageView imageView = holder.iv_featuredImage;
        TextView textView1, textView2,textView3;

        textView1 = holder.tv_featuredPrice;
        textView2 = holder.tv_featuredDisPrice;
        textView3 = holder.tv_featuredItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + featuredMddel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_featuredImage);
        double unit_price =  Double.parseDouble(featuredMddel.getUnit_price());
        double discount =  Double.parseDouble(featuredMddel.getDiscount());
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + featuredMddel.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (featuredMddel.getName().length() <= 24){
            textView3.setText(featuredMddel.getName());
        }else{
            textView3.setText(featuredMddel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_featuredImage;
        TextView tv_featuredPrice,tv_featuredDisPrice, tv_featuredItemName;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_featuredImage = (ImageView) itemView.findViewById(R.id.iv_featuredImage);
            tv_featuredPrice = (TextView) itemView.findViewById(R.id.tv_featuredPrice);
            tv_featuredDisPrice = (TextView) itemView.findViewById(R.id.tv_featuredDisPrice);
            tv_featuredItemName = (TextView) itemView.findViewById(R.id.tv_featuredItemName);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
