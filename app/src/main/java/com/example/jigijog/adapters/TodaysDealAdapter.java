package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Todaysdeal;

import java.util.ArrayList;



public class TodaysDealAdapter extends RecyclerView.Adapter<TodaysDealAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Todaysdeal> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public TodaysDealAdapter(Context context, ArrayList<Todaysdeal> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_todays_deal,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Todaysdeal todaysdealModel = mList.get(position);
         ImageView imageView = holder.iv_todaysdealImage;
         TextView textView1, textView2;

        textView1 = holder.tv_todaysdealPrice;
        textView2 = holder.tv_todaysdealPrice2;
        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + todaysdealModel.getPhotos())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_todaysdealImage);
        double unit_price =  Double.parseDouble(todaysdealModel.getUnit_price());
        double discount =  Double.parseDouble(todaysdealModel.getDiscount());
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + todaysdealModel.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_todaysdealImage;
        TextView tv_todaysdealPrice,tv_todaysdealPrice2;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_todaysdealImage = (ImageView) itemView.findViewById(R.id.iv_todaysdealImage);
            tv_todaysdealPrice = (TextView) itemView.findViewById(R.id.tv_todaysdealPrice);
            tv_todaysdealPrice2 = (TextView) itemView.findViewById(R.id.tv_todaysdealPrice2);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
