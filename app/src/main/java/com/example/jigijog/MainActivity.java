package com.example.jigijog;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import com.example.jigijog.helpers.CartContract;
import com.example.jigijog.models.User;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.UserRole;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

import static java.lang.StrictMath.max;

public class MainActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private String role;
    private FragmentManager fragmentManager;

    String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_chat, R.id.navigation_cart, R.id.navigation_account)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        context = this;
        role = UserRole.getRole(context);
        checkUserRole();
    }


    private void checkUserRole() {
        if (!role.isEmpty()) {
            checkCustomerSession();
        }else{

        }

    }


    public void checkCustomerSession() {
        view = findViewById(android.R.id.content);
        String userID = UserCustomer.getID(context);
        String userTokens = UserCustomer.getRememberToken(context);
        Debugger.logD("MAIN userID " + userID);
        Debugger.logD("MAIN userTokens " + userTokens);
        if(userID.equals("88")){
            loginUser();
        }else{
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);
            //fillUserTable();
        }

    }
    private void loginUser(){
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", UserCustomer.getEmail(context));
        params.put("password", UserCustomer.getPassword(context));

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD("str " + str);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    String remember_token = jsonObject.getString("remember_token");

                    UserCustomer userCustomer = new UserCustomer();
                    userCustomer.setId(id);
                    userCustomer.setRemember_token(remember_token);

                    getUserDetails(userCustomer);

                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void getUserDetails(final UserCustomer userCustomer){

        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("id", userCustomer.getId());
        params.put("remember_token", userCustomer.getRemember_token());
        Debugger.logD("id " + userCustomer.getId());
        Debugger.logD("remember_token " + userCustomer.getRemember_token());

        HttpProvider.post(context, "/mobile/getUserDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Debugger.logD("FUCK " + str);

                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");
                    String referred_by = jsonObject.getString("referred_by");
                    String refer_points = jsonObject.getString("refer_points");
                    String ref_id_status = jsonObject.getString("ref_id_status");
                    String email_verified_at = jsonObject.getString("email_verified_at");
                    String password = jsonObject.getString("password");
                    String remember_token = jsonObject.getString("remember_token");
                    String avatar = jsonObject.getString("avatar");
                    String avatar_original = jsonObject.getString("avatar_original");
                    String address = jsonObject.getString("address");
                    String country = jsonObject.getString("country");
                    String city = jsonObject.getString("city");
                    String barangay = jsonObject.getString("barangay");
                    String landmark = jsonObject.getString("landmark");
                    String postal_code = jsonObject.getString("postal_code");
                    String phone = jsonObject.getString("phone");
                    String bank_name = jsonObject.getString("bank_name");
                    String account_name = jsonObject.getString("account_name");
                    String account_number = jsonObject.getString("account_number");
                    String balance = jsonObject.getString("balance");
                    String created_at = jsonObject.getString("created_at");
                    String updated_at = jsonObject.getString("updated_at");

                    userCustomer.setId(id);
                    userCustomer.setProvider_id(provider_id);
                    userCustomer.setUser_type(user_type);
                    userCustomer.setName(name);
                    userCustomer.setEmail(email);
                    userCustomer.setReferred_by(referred_by);
                    userCustomer.setReferred_points(refer_points);
                    userCustomer.setRef_id_status(ref_id_status);
                    userCustomer.setEmail_verified_at(email_verified_at);
                    userCustomer.setPassword(UserCustomer.getPassword(context));
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setAvatar(avatar);
                    userCustomer.setAvatar_original(avatar_original);
                    userCustomer.setAddress(address);
                    userCustomer.setCountry(country);
                    userCustomer.setCity(city);
                    userCustomer.setBarangay(barangay);
                    userCustomer.setLandmark(landmark);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setPostal_code(postal_code);
                    userCustomer.setPhone(phone);
                    userCustomer.setBank_name(bank_name);
                    userCustomer.setAccount_name(account_name);
                    userCustomer.setAccount_number(account_number);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setBalance(balance);
                    userCustomer.setCreated_at(created_at);
                    userCustomer.setUpdated_at(updated_at);

                    UserRole userRole = new UserRole();
                    userRole.setUserRole(UserRole.Customer());
                    userRole.saveRole(context);

                    Debugger.logD("FUCK");
                    userCustomer.saveUserSession(context);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void fillUserTable() {
        User q1 = new User(Integer.parseInt(UserCustomer.getID(context)),  UserCustomer.getName(context));

        insertUserTable(q1);
    }
    @SuppressLint("WrongConstant")
    private void insertUserTable(User user ) {
        SQLiteDatabase db;
        db = openOrCreateDatabase("MyCart.db", SQLiteDatabase.CREATE_IF_NECESSARY
                , null);
        ContentValues cv = new ContentValues();
        cv.put(CartContract.UserTable.COLUMN_ID, user.getUser_id());
        cv.put(CartContract.UserTable.COLUMN_NAME, user.getName());
        db.insert(CartContract.UserTable.TABLE_NAME, null, cv);
    }

}
