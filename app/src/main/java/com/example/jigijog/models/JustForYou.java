package com.example.jigijog.models;

public class JustForYou {
    private String id;
    private String name;
    private String added_by;
    private String category_id;
    private String subcategory_id;
    private String subsubcategory_id;
    private String brand_id;
    private String photos;
    private String thumbnail_img;
    private String featured_img;
    private String flash_deal_img;
    private String video_provider;
    private String video_link;
    private String tags;
    private String description;
    private String unit_price;
    private String purchase_price;
    private String choice_options;
    private String colors;
    private String variations;
    private String todays_deal;
    private String published;
    private String featured;
    private String current_stock;
    private String unit;
    private String discount;
    private String discount_type;
    private String tax;
    private String tax_type;
    private String shipping_type;
    private String shipping_cost;
    private String weight;
    private String parcel_size;
    private String num_of_sale;
    private String meta_title;
    private String meta_description;
    private String meta_img;
    private String pdf;
    private String slug;
    private String rating;
    private String created_at;
    private String updated_at;

    public JustForYou(String id, String name, String added_by, String category_id, String subcategory_id, String subsubcategory_id, String brand_id, String photos, String thumbnail_img, String featured_img, String flash_deal_img, String video_provider, String video_link, String tags, String description, String unit_price, String purchase_price, String choice_options, String colors, String variations, String todays_deal, String published, String featured, String current_stock, String unit, String discount, String discount_type, String tax, String tax_type, String shipping_type, String shipping_cost, String weight, String parcel_size, String num_of_sale, String meta_title, String meta_description, String meta_img, String pdf, String slug, String rating, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.added_by = added_by;
        this.category_id = category_id;
        this.subcategory_id = subcategory_id;
        this.subsubcategory_id = subsubcategory_id;
        this.brand_id = brand_id;
        this.photos = photos;
        this.thumbnail_img = thumbnail_img;
        this.featured_img = featured_img;
        this.flash_deal_img = flash_deal_img;
        this.video_provider = video_provider;
        this.video_link = video_link;
        this.tags = tags;
        this.description = description;
        this.unit_price = unit_price;
        this.purchase_price = purchase_price;
        this.choice_options = choice_options;
        this.colors = colors;
        this.variations = variations;
        this.todays_deal = todays_deal;
        this.published = published;
        this.featured = featured;
        this.current_stock = current_stock;
        this.unit = unit;
        this.discount = discount;
        this.discount_type = discount_type;
        this.tax = tax;
        this.tax_type = tax_type;
        this.shipping_type = shipping_type;
        this.shipping_cost = shipping_cost;
        this.weight = weight;
        this.parcel_size = parcel_size;
        this.num_of_sale = num_of_sale;
        this.meta_title = meta_title;
        this.meta_description = meta_description;
        this.meta_img = meta_img;
        this.pdf = pdf;
        this.slug = slug;
        this.rating = rating;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public JustForYou() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAdded_by() {
        return added_by;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public String getSubsubcategory_id() {
        return subsubcategory_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public String getPhotos() {
        return photos;
    }

    public String getThumbnail_img() {
        return thumbnail_img;
    }

    public String getFeatured_img() {
        return featured_img;
    }

    public String getFlash_deal_img() {
        return flash_deal_img;
    }

    public String getVideo_provider() {
        return video_provider;
    }

    public String getVideo_link() {
        return video_link;
    }

    public String getTags() {
        return tags;
    }

    public String getDescription() {
        return description;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public String getChoice_options() {
        return choice_options;
    }

    public String getColors() {
        return colors;
    }

    public String getVariations() {
        return variations;
    }

    public String getTodays_deal() {
        return todays_deal;
    }

    public String getPublished() {
        return published;
    }

    public String getFeatured() {
        return featured;
    }

    public String getCurrent_stock() {
        return current_stock;
    }

    public String getUnit() {
        return unit;
    }

    public String getDiscount() {
        return discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public String getTax() {
        return tax;
    }

    public String getTax_type() {
        return tax_type;
    }

    public String getShipping_type() {
        return shipping_type;
    }

    public String getShipping_cost() {
        return shipping_cost;
    }

    public String getWeight() {
        return weight;
    }

    public String getParcel_size() {
        return parcel_size;
    }

    public String getNum_of_sale() {
        return num_of_sale;
    }

    public String getMeta_title() {
        return meta_title;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public String getMeta_img() {
        return meta_img;
    }

    public String getPdf() {
        return pdf;
    }

    public String getSlug() {
        return slug;
    }

    public String getRating() {
        return rating;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public void setSubsubcategory_id(String subsubcategory_id) {
        this.subsubcategory_id = subsubcategory_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public void setThumbnail_img(String thumbnail_img) {
        this.thumbnail_img = thumbnail_img;
    }

    public void setFeatured_img(String featured_img) {
        this.featured_img = featured_img;
    }

    public void setFlash_deal_img(String flash_deal_img) {
        this.flash_deal_img = flash_deal_img;
    }

    public void setVideo_provider(String video_provider) {
        this.video_provider = video_provider;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public void setChoice_options(String choice_options) {
        this.choice_options = choice_options;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public void setVariations(String variations) {
        this.variations = variations;
    }

    public void setTodays_deal(String todays_deal) {
        this.todays_deal = todays_deal;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public void setCurrent_stock(String current_stock) {
        this.current_stock = current_stock;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public void setTax_type(String tax_type) {
        this.tax_type = tax_type;
    }

    public void setShipping_type(String shipping_type) {
        this.shipping_type = shipping_type;
    }

    public void setShipping_cost(String shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setParcel_size(String parcel_size) {
        this.parcel_size = parcel_size;
    }

    public void setNum_of_sale(String num_of_sale) {
        this.num_of_sale = num_of_sale;
    }

    public void setMeta_title(String meta_title) {
        this.meta_title = meta_title;
    }

    public void setMeta_description(String meta_description) {
        this.meta_description = meta_description;
    }

    public void setMeta_img(String meta_img) {
        this.meta_img = meta_img;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
