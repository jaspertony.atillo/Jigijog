package com.example.jigijog.models;

public class SubCategories {
    private String id;
    private String name;
    private String category_id;
    private String slug;
    private String created_at;
    private String updated_at;

    public SubCategories(String id, String name, String category_id, String slug, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.slug = slug;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public SubCategories() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
